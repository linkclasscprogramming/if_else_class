#include<stdio.h>
int main()
{
    int value_1, value_2, value_3;
    int value_buffer;
    //  input data from keyboard
    printf("input 1 : ");
    scanf("%d", &value_1);
    printf("input 2 : ");
    scanf("%d", &value_2);
    printf("input 3 : ");
    scanf("%d", &value_3);
    // setting buffer
    value_buffer = value_1;
    for (int i = 1; i <= 3; i++)
    {
        switch (i)
        {
        case 1:
            if(value_1 > value_2)
            {
                value_buffer = value_2;
                value_2 = value_1;
                value_1 = value_buffer;
            }
            break;
        case 2:
            if(value_1 > value_3)
            {
                value_buffer = value_3;
                value_3 = value_1;
                value_1 = value_buffer;
            }
        case 3:
            if(value_2 > value_3)
            {
                value_buffer = value_3;
                value_3 = value_2;
                value_2 = value_buffer;
            }
        }
    }
    printf("result from sort : %d, %d, %d", value_1, value_2, value_3);
    return 0;
}